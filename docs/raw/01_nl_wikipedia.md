# NL Wikipedia

Ref. to these downloadable files:

https://dumps.wikimedia.org/nlwiki/latest/

The "full" set of the Dutch wikipedia language could be:

```
$ wget https://dumps.wikimedia.org/nlwiki/latest/nlwiki-latest-pages-articles-multistream.xml.bz2

$ bzip2 -d nlwiki-latest-pages-articles-multistream.xml.bz2

$ ls -lh nlwiki-latest-pages-articles-multistream.xml
7.1G nlwiki-latest-pages-articles-multistream.xml


# Improved version, removed the references more cleanly; leads to complex sed
# One problem is that sed does not have a non-greedy mode, so inverse characters
# need to be found to avoid greedyness ...
$ cat nlwiki-latest-pages-articles-multistream.xml  | \
  grep -v '^ *[<{|}=!&°#:\[\*\-]' | \
  grep -v '^(\*[*]*)' | \
  sed 's/([^()]*)\s*//g' | \
  sed 's/([^()]*)\s*//g' | \
  sed 's/&amp;nbsp;/ /g' | \
  sed 's/&quot;//g' | \
  sed 's/&amp;/ and /g' | \
  sed 's/&lt;!--[^&]*--&gt;//g' | \
  sed 's/&lt;br&gt;//g' | \
  sed s'/&lt;br[ ]*\/&gt;//g' | \
  sed 's/&lt;ref[^\[]*&gt;[^\[]*\[[^\[]*\][^\[]*&lt;\/ref&gt;//g' | \
  sed 's/&lt;ref[^\[]*&gt;[^\[]*\[[^\[]*\][^&]*&lt;\/ref&gt;//g' | \
  sed 's/&lt;ref[^\[]*&gt;[^\[]*\[\[[^\[]*\]\][^\[]*&lt;\/ref&gt;//g' | \
  sed 's/&lt;ref[^\[]*&gt;{{Citeer[^}]*&lt;\/ref&gt;//g' | \
  sed 's/&lt;ref[^&]*&gt;[^&]*&lt;\/ref&gt;//g' | \
  sed 's/\.&lt;ref[^&]*&gt;.*lt;\/ref&gt;//g' | \
  sed 's/\[\[[^]]*|\([^]]*\)\]\]/[[\1]]/g' | \
  sed 's/\.&lt;ref&gt;{{Citeer.*//' | \
  sed 's/&lt;ref&gt;.*//' | \
  sed 's/.*&lt;\/ref&gt;\s*//' | \
  sed 's/{{[^}]*}}//g' | \
  sed 's/\[\[\([^]]*\)\]\]/\1/g'| \
  sed 's/&lt;ref name\=[^&]*\/&gt;//g' | \
  grep -v '^ id:' | \
  grep -v '^ bar:' | \
  grep -v '^Bestand:' | \
  grep -v '=' | \
  grep -v '^$' | \
  grep -v '^ ' | \
  sed "s/\(''[']*\)\([^']*\)\1/\2/g" | \
  sed "s/\s*''[']*//g" | \
  grep -v '\[\[' | \
  grep -v '\]\]' | \
  sed 's/\([.!?:]\)\s\s*$/\1/' | \
  grep '[.!?]$' > clean-text-nlwiki.txt

$ wc clean-text-nlwiki.txt
  5257845  229956424 1465847126 clean-text-nlwiki.txt

# that is 5.2 M paragraphs, with 220M words
```

To further transform the paragraph to sentences, one could use
a heuristic split on '.' (but this also splits incorrectly on
abbreviations etc. ... trying to remove a few cases by expanding
the abbreviations). And at the end, brute force removing short
sentences, since these have most of the trouble.

```
$ cat clean-text-nlwiki.txt | \
  sed 's/\([BN]\)\.V\./\1V/g' | \
  sed 's/\.\.\.//g' | \
  sed 's/ [ ]*/ /g' | \
  sed 's/\?\./?/g' | \
  sed 's/\s\s*\././' | \
  sed 's/Chr\./Christus/g' | \
  sed 's/ Inc\./ Incorporated/g' | \
  sed 's/ enz\./ enzovoort/g' | \
  sed 's/ art\./ artikel/g' | \
  sed 's/ bv\./ bijvoorbeeld/g' | \
  sed 's/ St\./ Sint/g' | \
  sed 's/ [iI]r\./ ingenieur/g' | \
  sed 's/ [pP]rof\./ Professor/g' | \
  sed 's/ [Dd]rs\./ doctorandus/g' | \
  sed 's/ [cC]a\./ circa/g' | \
  sed 's/ zgn\./ zogenaamde/g' | \
  sed 's/ lit\./ literatuur/g' | \
  sed 's/ resp\./ respectievelijk/g' | \
  sed 's/ No\./ number/g' | \
  sed 's/ mv\./ meervoud/g' | \
  sed 's/ plm\./ plus minus/g' | \
  sed 's/ dB\./ deciBel/g' | \
  sed 's/ [jJ]r\./ junior/g' | \
  sed 's/ [Ss]r\./ Senior/g' | \
  sed 's/ [dD]r\./ doctor/g' | \
  sed 's/ Mrs./ Missus/g' | \
  sed 's/ [Mm]ax\./ maximum/g' | \
  sed 's/ [Mm]in\./ minimum/g' | \
  sed 's/ etc\./ etcetera/g' | \
  sed 's/ Inw\./ Inwoners/g' | \
  sed 's/ Art\./ Artikel/g' | \
  sed 's/ Mr\./ Mister/g' | \
  sed 's/ Ms\./ Miss/g' | \
  sed 's/\([^. ].\)\.[ \$]/\1.\n/g' | \
  grep -v '^\?' | \
  grep -v 'http' |
  grep -v '^;' | \
  grep '^.\{15\}' > clean-sentences-nlwiki.txt

$ wc clean-sentences-nlwiki.txt
  14049901  228741534 1464016392 clean-sentences-nlwiki.txt

# that is 14 M sentences, with 220M words
```
(adding a `| sed '/.*/G' | less` allows easier manual inspection)
